# Modolino

## Description

Modolino is a communication platform for schools, kindergardens and parents that is...

- ...open Source.
- ...low Cost.
- ...helps exchange messages and data in a simple way.
- ...concentrated on a joint effort to protect childrens, families and social institutions privacy.

## Installation

This app is a web application and therefore runs without installation through an internet connection and a browser that is open to modolinos website url.

## Usage

Planned:

- Groups have a joined thread with messages.
- Groups have a designated space for shared data content.

### Privacy

Though security and privacy is top priority to this project, **as few sensitive data as ever possible should be shared inside the app ever** to minimize risk. Because, eventually, websites, even very secure ones can potentially be hacked.

## Support

Feel free to contact me on [GitLab](https://gitlab.com/cyaton).

## Feedback

Any feedback, suggestions or collaboration attempts are welcome! Especially for other projects!

## Roadmap

Set up initial framework to work with

## Authors and acknowledgment

Contributors: Philipp Montazem

### Acknowledgments

#### Mozilla

As always, thanks to Mozilla for providing the [mdn resources for developers](https://developer.mozilla.org/en-US/).

## LICENSE

Copyright (C) 2023 Philipp Montazem

This Software is published under the GNU Affero General Public License v3.0. Please see [LICENSE.md](LICENSE.md) for this softwares specific license conditions.

## Project status

Active development. Readme last updated on October 9th, 2023